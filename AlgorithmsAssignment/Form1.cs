﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgorithmsAssignment
{
    public partial class Form1 : Form
    {
        public int[] globalArr = new int[100000];
        public int[] insertionSortedArr = new int[100000];
        public int[] selectionSortedArr = new int[100000];
        public int[] bubbleSortedArr = new int[100000];
        public int[] mergeSortedArr = new int[100000];
        public int[] heapSortedArr = new int[100000];
        public int[] quickSortedArr = new int[100000];
        public int tick = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void createArray_Click(object sender, EventArgs e)
        {
            int min = Int32.Parse(minValue.Text);
            int max = Int32.Parse(maxValue.Text);
            Random randNum = new Random();
            for (int i = 0; i < 100000; i++)
            {
                globalArr[i]= randNum.Next(min, max);
            }
            
            arrayCommand.Text = "Array created.";
            Console.WriteLine(globalArr[300]);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }

        private void insertionSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;

           for (int i = 0; i < 10; i++)
           {
                Thread T = new Thread(insertionSortThread, 10000000);
                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds; ;
                T.Abort();
                double total_time = local_end_time - local_start_time;
               bigTime += total_time;
                stateConsole.Text += "\nInsertion Sort applied. Time: " + total_time + " miliseconds";
            }

            stateConsole.Text += "\nAverage time for 10 Insertion Sorts: " + (bigTime/10) + " miliseconds";
        }

        private void selectionSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;

            for (int i = 0; i < 10; i++)
            {
                Thread T = new Thread(selectionSortThread, 10000000);
                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds; ;
                T.Abort();
                double total_time = local_end_time - local_start_time;
                bigTime += total_time;
                stateConsole.Text += "\nSelection Sort applied. Time: " + total_time + " miliseconds";
            }
            stateConsole.Text += "\nAverage time for 10 Selection Sorts: " + (bigTime / 10) + " miliseconds";
        }

        private void bubbleSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;

            for (int i = 0; i < 10; i++)
            {
                Thread T = new Thread(bubbleSortThread, 10000000);


                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds; ;
                T.Abort();
                double total_time = local_end_time - local_start_time;
                bigTime += total_time;
                stateConsole.Text += "\nBubble Sort applied.  Time: " + total_time + " miliseconds";
            }
            stateConsole.Text += "\nAverage time for 10 Bubble Sorts: " + (bigTime / 10) + " miliseconds";
        }

        private void mergeSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;

            for (int i = 0; i < 10; i++)
            {
                Thread T = new Thread(mergeSortThread, 10000000);

                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds; ;
                T.Abort();
                double total_time = local_end_time - local_start_time;
                bigTime += total_time;
                stateConsole.Text += "\nMerge Sort applied.  Time: " + total_time + " miliseconds";
            }
            stateConsole.Text += "\nAverage time for 10 Merge Sorts: " + (bigTime / 10) + " miliseconds";
        }

        private void heapSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;

            for (int i = 0; i < 10; i++)
            {
                Thread T = new Thread(heapSortThread, 10000000);

                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds; ;
                T.Abort();
                double total_time = local_end_time - local_start_time;
                bigTime += total_time;
                stateConsole.Text += "\nHeap Sort applied. Time: " + total_time + " miliseconds";
            }
            stateConsole.Text += "\nAverage time for 10 Heap Sorts: " + (bigTime / 10) + " miliseconds";
        }

        private void quickSortButton_Click(object sender, EventArgs e)
        {

            double bigTime = 0;
            for (int i = 0; i < 10; i++)
            {
                Thread T = new Thread(quickSortThread, 10000000);
                double local_start_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Start();
                T.Join();
                double local_end_time = (new TimeSpan(DateTime.Now.Ticks)).TotalMilliseconds;
                T.Abort();
                double total_time = local_end_time - local_start_time;
                bigTime += total_time;
                stateConsole.Text += "\nQuick Sort applied. " + total_time + " miliseconds";
            }
            stateConsole.Text += "\nAverage time for 10 Quick Sorts: " + (bigTime / 10) + " miliseconds";
        }

        private void sortTheRandomArray_Click(object sender, EventArgs e)
        {
            //sort
            Array.Sort(globalArr); 
            stateConsole.Text += "\nOur main array resorted. If you press the algorithm buttons, the algorithms are gonna try to run on already sorted array. " ;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void descendTheRandomArray_Click(object sender, EventArgs e)
        {
            //sort
            Array.Sort(globalArr);
            // Descending order
            Array.Reverse(globalArr);
            stateConsole.Text += "\nOur main array is now in descended order. If you press the algorithm buttons, the algorithms are gonna try to run on reverse ordered array. ";
        }
        public void heapSortThread()
        {
            int[] heapArray = new int[100000];
            HeapSort heapSort = new HeapSort();
            Array.Copy(globalArr, heapArray, 100000);
            heapSortedArr = heapSort.Sort(heapArray);
        }
        public void mergeSortThread()
        {
            int[] mergeArray = new int[100000];
            MergeSortSort merSort = new MergeSortSort();
            Array.Copy(globalArr, mergeArray, 100000);
            mergeSortedArr = merSort.Sort(mergeArray);
        }
        public void bubbleSortThread()
        {
            int[] bubbleArray = new int[100000];
            BubbleSort bubSort = new BubbleSort();
            Array.Copy(globalArr, bubbleArray, 100000);
            bubbleSortedArr = bubSort.Sort(bubbleArray);
        }

        public void selectionSortThread()
        {
            int[] selectionArray = new int[100000];
            SelectionSort selSort = new SelectionSort();
            Array.Copy(globalArr, selectionArray, 100000);
            selectionSortedArr = selSort.Sort(selectionArray);

        }

        public void insertionSortThread()
        {
            int[] insertionArray = new int[100000];
            InsertionSort inSort = new InsertionSort();
            Array.Copy(globalArr, insertionArray, 100000);
            insertionSortedArr = inSort.Sort(insertionArray);
        }
        public void quickSortThread()
        {
            int[] quickArray = new int[100000];
            Array.Copy(globalArr, quickArray, 100000);
            QuickSort quickSort = new QuickSort();
            
            quickSortedArr = quickSort.Sort(quickArray);

        }

        private void displayButton_Click(object sender, EventArgs e)
        {
            if (chooseBox.SelectedIndex == 0)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n"+ (i + 1) + ". Element of the Random Array: "+globalArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 1)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Insertıon Sort Array: " + insertionSortedArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 2)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Bubble Sort Array: " + bubbleSortedArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 3)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Selection Sort Array: " + selectionSortedArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 4)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Merge Sort Array: " + mergeSortedArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 5)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Heap Sort Array: " + heapSortedArr[i];
                }

            }
            if (chooseBox.SelectedIndex == 6)
            {
                int forCount = Int32.Parse(elementCount.Text);
                for (int i = 0; i < forCount; i++)
                {
                    stateConsole.Text += "\n" + (i + 1) + ". Element of the Quick Sort Array: " + quickSortedArr[i];
                }

            }
        }
    }
}
