﻿namespace AlgorithmsAssignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createArray = new System.Windows.Forms.Button();
            this.arrayCommand = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.minValue = new System.Windows.Forms.TextBox();
            this.maxValue = new System.Windows.Forms.TextBox();
            this.stateConsole = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.insertionSortButton = new System.Windows.Forms.Button();
            this.selectionSortButton = new System.Windows.Forms.Button();
            this.bubbleSortButton = new System.Windows.Forms.Button();
            this.mergeSortButton = new System.Windows.Forms.Button();
            this.heapSortButton = new System.Windows.Forms.Button();
            this.quickSortButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sortTheRandomArray = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.descendTheRandomArray = new System.Windows.Forms.Button();
            this.chooseBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.elementCount = new System.Windows.Forms.TextBox();
            this.displayButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // createArray
            // 
            this.createArray.Location = new System.Drawing.Point(268, 6);
            this.createArray.Name = "createArray";
            this.createArray.Size = new System.Drawing.Size(175, 43);
            this.createArray.TabIndex = 0;
            this.createArray.Text = "Create Ranedom Array";
            this.createArray.UseVisualStyleBackColor = true;
            this.createArray.Click += new System.EventHandler(this.createArray_Click);
            // 
            // arrayCommand
            // 
            this.arrayCommand.Location = new System.Drawing.Point(445, 6);
            this.arrayCommand.Name = "arrayCommand";
            this.arrayCommand.ReadOnly = true;
            this.arrayCommand.Size = new System.Drawing.Size(202, 20);
            this.arrayCommand.TabIndex = 1;
            this.arrayCommand.Text = "Array did not created.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Minimum Value of the Random Array:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Maximum Value of the Random Array:";
            // 
            // minValue
            // 
            this.minValue.Location = new System.Drawing.Point(199, 6);
            this.minValue.Name = "minValue";
            this.minValue.Size = new System.Drawing.Size(63, 20);
            this.minValue.TabIndex = 4;
            this.minValue.Text = "0";
            // 
            // maxValue
            // 
            this.maxValue.Location = new System.Drawing.Point(199, 29);
            this.maxValue.Name = "maxValue";
            this.maxValue.Size = new System.Drawing.Size(63, 20);
            this.maxValue.TabIndex = 5;
            this.maxValue.Text = "100000";
            // 
            // stateConsole
            // 
            this.stateConsole.Location = new System.Drawing.Point(12, 77);
            this.stateConsole.Name = "stateConsole";
            this.stateConsole.ReadOnly = true;
            this.stateConsole.Size = new System.Drawing.Size(431, 361);
            this.stateConsole.TabIndex = 6;
            this.stateConsole.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(12, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "State Console";
            // 
            // insertionSortButton
            // 
            this.insertionSortButton.Location = new System.Drawing.Point(469, 77);
            this.insertionSortButton.Name = "insertionSortButton";
            this.insertionSortButton.Size = new System.Drawing.Size(96, 23);
            this.insertionSortButton.TabIndex = 8;
            this.insertionSortButton.Text = "Insertion Sort";
            this.insertionSortButton.UseVisualStyleBackColor = true;
            this.insertionSortButton.Click += new System.EventHandler(this.insertionSortButton_Click);
            // 
            // selectionSortButton
            // 
            this.selectionSortButton.Location = new System.Drawing.Point(469, 106);
            this.selectionSortButton.Name = "selectionSortButton";
            this.selectionSortButton.Size = new System.Drawing.Size(96, 23);
            this.selectionSortButton.TabIndex = 9;
            this.selectionSortButton.Text = "Selection Sort";
            this.selectionSortButton.UseVisualStyleBackColor = true;
            this.selectionSortButton.Click += new System.EventHandler(this.selectionSortButton_Click);
            // 
            // bubbleSortButton
            // 
            this.bubbleSortButton.Location = new System.Drawing.Point(571, 77);
            this.bubbleSortButton.Name = "bubbleSortButton";
            this.bubbleSortButton.Size = new System.Drawing.Size(96, 23);
            this.bubbleSortButton.TabIndex = 10;
            this.bubbleSortButton.Text = "Bubble Sort";
            this.bubbleSortButton.UseVisualStyleBackColor = true;
            this.bubbleSortButton.Click += new System.EventHandler(this.bubbleSortButton_Click);
            // 
            // mergeSortButton
            // 
            this.mergeSortButton.Location = new System.Drawing.Point(571, 106);
            this.mergeSortButton.Name = "mergeSortButton";
            this.mergeSortButton.Size = new System.Drawing.Size(96, 23);
            this.mergeSortButton.TabIndex = 12;
            this.mergeSortButton.Text = "Merge Sort";
            this.mergeSortButton.UseVisualStyleBackColor = true;
            this.mergeSortButton.Click += new System.EventHandler(this.mergeSortButton_Click);
            // 
            // heapSortButton
            // 
            this.heapSortButton.Location = new System.Drawing.Point(469, 135);
            this.heapSortButton.Name = "heapSortButton";
            this.heapSortButton.Size = new System.Drawing.Size(96, 23);
            this.heapSortButton.TabIndex = 13;
            this.heapSortButton.Text = "Heap Sort";
            this.heapSortButton.UseVisualStyleBackColor = true;
            this.heapSortButton.Click += new System.EventHandler(this.heapSortButton_Click);
            // 
            // quickSortButton
            // 
            this.quickSortButton.Location = new System.Drawing.Point(567, 135);
            this.quickSortButton.Name = "quickSortButton";
            this.quickSortButton.Size = new System.Drawing.Size(96, 23);
            this.quickSortButton.TabIndex = 14;
            this.quickSortButton.Text = "Quick Sort";
            this.quickSortButton.UseVisualStyleBackColor = true;
            this.quickSortButton.Click += new System.EventHandler(this.quickSortButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(498, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "PHASE 1: Sort Random Array";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(459, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(208, 15);
            this.label5.TabIndex = 16;
            this.label5.Text = "PHASE 2: Switch Random Array to Sorted";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // sortTheRandomArray
            // 
            this.sortTheRandomArray.Location = new System.Drawing.Point(479, 179);
            this.sortTheRandomArray.Name = "sortTheRandomArray";
            this.sortTheRandomArray.Size = new System.Drawing.Size(157, 23);
            this.sortTheRandomArray.TabIndex = 17;
            this.sortTheRandomArray.Text = "Sort the Random Array";
            this.sortTheRandomArray.UseVisualStyleBackColor = true;
            this.sortTheRandomArray.Click += new System.EventHandler(this.sortTheRandomArray_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(442, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(232, 15);
            this.label6.TabIndex = 18;
            this.label6.Text = "PHASE 3: Switch Random Array to Descended";
            // 
            // descendTheRandomArray
            // 
            this.descendTheRandomArray.Location = new System.Drawing.Point(469, 223);
            this.descendTheRandomArray.Name = "descendTheRandomArray";
            this.descendTheRandomArray.Size = new System.Drawing.Size(167, 23);
            this.descendTheRandomArray.TabIndex = 19;
            this.descendTheRandomArray.Text = "Descend the Random Array";
            this.descendTheRandomArray.UseVisualStyleBackColor = true;
            this.descendTheRandomArray.Click += new System.EventHandler(this.descendTheRandomArray_Click);
            // 
            // chooseBox
            // 
            this.chooseBox.FormattingEnabled = true;
            this.chooseBox.Items.AddRange(new object[] {
            "Random Array",
            "Instertion Sort Array",
            "Bubble Sort Array",
            "Selection Sort Array",
            "Merge Sort Array",
            "Heap Sort Array",
            "Quick Sort Array"});
            this.chooseBox.Location = new System.Drawing.Point(442, 281);
            this.chooseBox.Name = "chooseBox";
            this.chooseBox.Size = new System.Drawing.Size(121, 21);
            this.chooseBox.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(442, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Display Array";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(564, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "How Many Elements?";
            // 
            // elementCount
            // 
            this.elementCount.Location = new System.Drawing.Point(567, 281);
            this.elementCount.Name = "elementCount";
            this.elementCount.Size = new System.Drawing.Size(100, 20);
            this.elementCount.TabIndex = 23;
            this.elementCount.Text = "10";
            // 
            // displayButton
            // 
            this.displayButton.Location = new System.Drawing.Point(442, 308);
            this.displayButton.Name = "displayButton";
            this.displayButton.Size = new System.Drawing.Size(225, 23);
            this.displayButton.TabIndex = 24;
            this.displayButton.Text = "Display";
            this.displayButton.UseVisualStyleBackColor = true;
            this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(675, 450);
            this.Controls.Add(this.displayButton);
            this.Controls.Add(this.elementCount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chooseBox);
            this.Controls.Add(this.descendTheRandomArray);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sortTheRandomArray);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.quickSortButton);
            this.Controls.Add(this.heapSortButton);
            this.Controls.Add(this.mergeSortButton);
            this.Controls.Add(this.bubbleSortButton);
            this.Controls.Add(this.selectionSortButton);
            this.Controls.Add(this.insertionSortButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.stateConsole);
            this.Controls.Add(this.maxValue);
            this.Controls.Add(this.minValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.arrayCommand);
            this.Controls.Add(this.createArray);
            this.Name = "Form1";
            this.Text = "Analysis of Algorithms Assignmnet App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createArray;
        private System.Windows.Forms.TextBox arrayCommand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox minValue;
        private System.Windows.Forms.TextBox maxValue;
        private System.Windows.Forms.RichTextBox stateConsole;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button insertionSortButton;
        private System.Windows.Forms.Button selectionSortButton;
        private System.Windows.Forms.Button bubbleSortButton;
        private System.Windows.Forms.Button mergeSortButton;
        private System.Windows.Forms.Button heapSortButton;
        private System.Windows.Forms.Button quickSortButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button sortTheRandomArray;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button descendTheRandomArray;
        private System.Windows.Forms.ComboBox chooseBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox elementCount;
        private System.Windows.Forms.Button displayButton;
    }
}

