﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsAssignment
{
    class QuickSort
    {
        public void Quick_Sort(int[] dizi, int baslangic, int bitis)
        {
            int i;
            if (baslangic < bitis)
            {
                i = partition(dizi, baslangic, bitis);
                Quick_Sort(dizi, baslangic, i-1);
                Quick_Sort(dizi, i + 1, bitis);
            }
        }
        public int partition(int[] A, int baslangic, int bitis)
        {
            int gecici;
            int x = A[bitis];
            int i = baslangic - 1;
            for (int j = baslangic; j <= bitis - 1; j++)
            {
                if (A[j] <= x)
                {
                    i++;
                    gecici = A[i];
                    A[i] = A[j];
                    A[j] = gecici;
                }
            }
            gecici = A[i + 1];
            A[i + 1] = A[bitis];
            A[bitis] = gecici;
            return i + 1;
        }
        public int[] Sort(int[] array)
        {
            int[] arr = array;
            int n = arr.Length;
            Quick_Sort(arr, 0, arr.Length - 1);
            return arr;
        }
    }
}
